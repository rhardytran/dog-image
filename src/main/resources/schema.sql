-- dog image table
DROP TABLE IF EXISTS dog_image;

CREATE TABLE dog_image (
 	id INT AUTO_INCREMENT PRIMARY KEY,
 	breed VARCHAR(100) NOT NULL,
 	image_url VARCHAR(250),
 	create_at TIMESTAMP DEFAULT now(),
 	modify_at TIMESTAMP DEFAULT now()
);
