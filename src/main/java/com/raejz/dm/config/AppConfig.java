package com.raejz.dm.config;

import com.raejz.dm.bus.greet.GreetResponse;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Configuration
public class AppConfig {
    @Bean
    public Clock getClock() {
        return Clock.systemUTC();
    }

    @Bean
    public GreetResponse greeting() {
        GreetResponse greetResponse = new GreetResponse();
        greetResponse.setAppName("Dog Image Management System");
        greetResponse.setMessage("Greetings from Dog Image Management System!");
        LocalDateTime dateAndTime = LocalDateTime.now(getClock());
        greetResponse.setStartupDate(dateAndTime.format(DateTimeFormatter.ISO_DATE_TIME));

        return greetResponse;
    }
}
