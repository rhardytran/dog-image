package com.raejz.dm.bus.greet;

import com.raejz.dm.bus.API;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GreetController {
    private static final Logger logger = LoggerFactory.getLogger(GreetController.class);

    @NonNull
    private GreetResponse greetResponse;

    @GetMapping(value = API.V1 + "/greet", produces = MediaType.APPLICATION_JSON_VALUE)
    public GreetResponse index() {
        logger.info("Greeting from Dog Image Management System");
        return greetResponse;
    }
}
