package com.raejz.dm.bus.greet;

import lombok.Data;

@Data
public class GreetResponse {
    private String appName;
    private String message;
    private String startupDate;
}
