package com.raejz.dm.bus.dog;

import com.raejz.dm.common.repo.BaseRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/*
    Jpa repository has methods like save(), findOne(), findAll(), count(), delete(), etc.
 */
@Repository
public interface DogImageRepository extends JpaRepository<DogImageEntity, Long> {
}
