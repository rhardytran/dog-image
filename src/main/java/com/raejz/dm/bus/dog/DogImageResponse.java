package com.raejz.dm.bus.dog;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Column;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DogImageResponse {
    private String id;
    private String breed;
    // @JsonProperty("IMAGE_URL")
    private String imageUrl;
}
