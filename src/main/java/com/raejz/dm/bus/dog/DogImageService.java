package com.raejz.dm.bus.dog;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DogImageService {
    private static final Logger logger = LoggerFactory.getLogger(DogImageService.class);

    @NonNull
    private final DogImageRepository dogImageRepository;

    public List<DogImageEntity> getAll() {
        logger.info("Retrieve all dog images");
        return dogImageRepository.findAll();
    }

    public DogImageEntity getById(Long id) {
        logger.info("Retrieve all dog images");
        return dogImageRepository.findById(id).orElse(null);
    }

    public DogImageEntity add(DogImageEntity dogImageEntity) {
        logger.info("Save dog images");
        return dogImageRepository.save(dogImageEntity);
    }

    public void delete(DogImageEntity dogImageEntity) {
        logger.info("Delete dog images");
        dogImageRepository.delete(dogImageEntity);
    }
}
