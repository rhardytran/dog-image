package com.raejz.dm.bus.dog;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DogImageRequest {
    private String id;
    private String breed;
    private String imageUrl;
}
