package com.raejz.dm.bus.dog;

import com.raejz.dm.bus.API;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping(value = API.V1 + "/dog-images")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DogImageController {

    @NotNull
    private final DogImageService dogImageService;

    @NonNull
    private final DogImageMapper dogImageMapper;

    @RequestMapping(value = "/all", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public DeferredResult<ResponseEntity<?>> getAllDogImages() {
        DeferredResult<ResponseEntity<?>> deferredResult = new DeferredResult<>();
        List<DogImageEntity> dogImageEntities = dogImageService.getAll();

        deferredResult.setResult(ResponseEntity.ok(dogImageMapper.toDogImageResponses(dogImageEntities)));
        return deferredResult;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public DeferredResult<ResponseEntity<?>> getDogImageById(@PathVariable Long id, HttpServletRequest request) {
        DeferredResult<ResponseEntity<?>> deferredResult = new DeferredResult<>();

        DogImageEntity dogImageEntity = dogImageService.getById(id);
        if (dogImageEntity == null) {
            ResponseEntity<?> responseEntity = new ResponseEntity<>("{}", HttpStatus.OK);
            deferredResult.setResult(responseEntity);
        } else {
            deferredResult.setResult(ResponseEntity.ok(dogImageMapper.toDogImageResponse(dogImageEntity)));
        }

        return deferredResult;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public DeferredResult<ResponseEntity<?>> createDogImage(@RequestBody DogImageRequest dogImageRequest) {
        DeferredResult<ResponseEntity<?>> deferredResult = new DeferredResult<>();
        DogImageEntity dogImageEntity = dogImageMapper.fromDogImageRequest(dogImageRequest);
        DogImageEntity insertDogImage = dogImageService.add(dogImageEntity);

        if (insertDogImage == null) {
            ResponseEntity<?> responseEntity = new ResponseEntity<>("{}", HttpStatus.OK);
            deferredResult.setResult(responseEntity);
        } else {
            deferredResult.setResult(ResponseEntity.ok(dogImageMapper.toDogImageResponse(insertDogImage)));
        }

        return deferredResult;
    }
}
