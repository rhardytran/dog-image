package com.raejz.dm.bus.dog;

import com.raejz.dm.common.model.BaseEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "DOG_IMAGE")
public class DogImageEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "ID")
    private Long id;
    @Column(name = "BREED")
    private String breed;

    @Column(name = "IMAGE_URL")
    private String imageUrl;

    @Column(name = "create_at")
    private Date createAt;

    @Column(name = "modify_at")
    private Date modifyAt;
}
