package com.raejz.dm.bus.dog;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DogImageMapper {
    DogImageResponse toDogImageResponse(DogImageEntity dogImageEntity);
    DogImageEntity fromDogImageResponse(DogImageResponse dogImageResponse);
    List<DogImageResponse> toDogImageResponses(List<DogImageEntity> dogImageEntities);

    DogImageEntity fromDogImageRequest(DogImageRequest dogImageRequest);

}
