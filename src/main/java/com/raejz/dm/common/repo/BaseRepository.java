package com.raejz.dm.common.repo;

import com.raejz.dm.common.model.BaseEntity;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.Clock;
import java.time.LocalDateTime;

/**
 *
 */
@Repository
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BaseRepository {
  @NonNull
  protected Clock clock;

  public BaseRepository() {
  }

  public void add(BaseEntity baseEntity) {
    Timestamp iso8601 = Timestamp.valueOf(LocalDateTime.now(clock));
    baseEntity.setCreateAt(iso8601);
    baseEntity.setModifyAt(iso8601);
  }

  public void modify(BaseEntity baseEntity) {
    baseEntity.setModifyAt(Timestamp.valueOf(LocalDateTime.now(clock)));
  }
}
