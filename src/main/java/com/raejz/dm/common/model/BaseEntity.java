package com.raejz.dm.common.model;

import javax.persistence.Column;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.Date;

public class BaseEntity implements Serializable {

    @Column(name = "create_at")
    private Date createAt;

    @Column(name = "modify_at")
    private Date modifyAt;

    public BaseEntity() {
    }

    public BaseEntity(Date createAt, Date modifyAt) {
        this.createAt = createAt;
        this.modifyAt = modifyAt;
    }


    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getModifyAt() {
        return modifyAt;
    }

    public void setModifyAt(Date modifyAt) {
        this.modifyAt = modifyAt;
    }
}