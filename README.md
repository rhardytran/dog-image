## Dog Picture Management System

### Goals
Create API for both internal & external to manage dog images.

### Frameworks

1. Java 8
2. Spring Boot 2.2.0
3. Mapstruct (Object Mapper)
5. Spring Test
6. Gradle 5.6.3

### Datastore
1. H2 Datatabase (In-Memory Datastore)

### Build jar file
1) change to project root <br/> 
    cd dog-mag<br/>
2) build jar <br/>
    gradle clean build

### Running Locally
`gradle bootRun`<br/>


**Application Status**

| Path                         | Method | Description                        | Response                     |
|------------------------------|--------|------------------------------------|------------------------------|
| /actuator/health    | GET    | get application health check       |  |
| /actuator/info    | GET    | get application info       |  |
| /actuator/metrics            | GET   | get application & endpoint metrics                   |                              |


**Dog Images**

| Path                         | Method | Description                        | Response                     |
|------------------------------|--------|------------------------------------|------------------------------|
| /v1/dog-images            | POST   | add new dog image                   |                              |
| /v1/dog-images/{id}       | GET    | get dog image by id                  | dog imageß json object          |
| /v1/dog-images/{id}       | PUT    | update dog image by id          |                              |
| /v1/dog-images/{id}       | DELETE | delete dog image by id               |                              |
